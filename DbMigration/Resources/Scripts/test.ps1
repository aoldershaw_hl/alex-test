# # Set Parameters
# param ($current_date_time, $secret_id, $cross_bucket, $source_bucket, $target_bucket, $region)

$current_date_time = "03-02-2021_04-19-00"
$secret_id = "oc/dev-templaterestore"
$cross_bucket = "dev-s3-dbmigration"
$prod_bucket = "dev-s3-dbmigration"

# Set current region
$default_region = "Set-DefaultAWSRegion -Region $region"
Invoke-Expression $default_region

# Pull secret with Db information
$secret_json = Get-SECSecretValue -SecretId $secret_id
$secret = $secret_json.SecretString | ConvertFrom-Json

# Set secret values as variables
$username = $secret.username
$password = $secret.password
$database_list = $secret.dblist.Split("-")
$host_name = $secret.host
$server = $secret.server
$dir_path = $secret.dirpath

# Set file names for latest backup and rollback
$backup_latest = $host_name + '_Latest'
$backup_rollback = $host_name + '_' + $current_date_time

# Create working directory
New-Item -Path $dir_path -Name "DbBackups" -ItemType "directory"

# Run sqlpackage extract on each Db
foreach ($database in $database_list)
{
    & "C:/Program Files/Microsoft SQL Server/150/DAC/bin/SqlPackage.exe" /Action:Extract /SourceServerName:$server /SourceDatabaseName:$database /SourceUser:$username /SourcePassword:$password /TargetFile:$dir_path\DbBackups\$database.dacpac /p:ExtractAllTableData=true /p:VerifyExtraction=false /p:CommandTimeout=300 /p:ExtractReferencedServerScopedElements=False /p:IgnoreUserLoginMappings=True
}

# Zip all backups
Compress-Archive -Path "$dir_path\DbBackups\*.*" -DestinationPath "$dir_path\DbBackups\DbBackups.zip"

# Push zipped backups to prod bucket S3 bucket /Latest & /Rollback
Write-S3Object -BucketName $target_bucket -File "$dir_path\DbBackups\DbBackups.zip" -Key "/Rollback/$backup_rollback.zip"

# Search for $backup_rollback.zip at rollback location
$uploaded_object = Get-S3Object -BucketName $target_bucket -Key "/Rollback/$backup_rollback.zip"

# If $backup_rollback.zip is present continue, else exit
if ( $uploaded_object )
{
    Write-Output "Backup file located in S3, restoring databases"

    # Clean up working directory of backed up files
    Remove-Item "$dir_path\DbBackups\*" -Recurse

    # Pull latest Dev Db backups to working directory
    Read-S3Object -BucketName $cross_bucket -Key "/Latest/Backup/$backup_latest.zip" -File "$dir_path\DbBackups\$backup_latest.zip"

    # Unzip Dev backups
    Expand-Archive -LiteralPath "$dir_path\DbBackups\$backup_latest.zip" -DestinationPath "$dir_path\DbBackups\"

    # # Restore all Dbs
    # foreach ($database in $database_list)
    # {
    #     Invoke-SqlCmd -ServerInstance $server -Database $database -Query 'EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all";'
    #     & "C:/Program Files/Microsoft SQL Server/150/DAC/bin/SqlPackage.exe" /Action:Publish /TargetServerName:$server /TargetDatabaseName:$database /TargetUser:$username /TargetPassword:$password /SourceFile:$dir_path"\DbBackups\$database.dacpac" /p:CreateNewDatabase=false /p:ScriptNewConstraintValidation=true /p:IgnoreWithNocheckOnForeignKeys=true /p:AllowIncompatiblePlatform=true /p:ExcludeObjectTypes='Users;Aggregates;ApplicationRoles;Assemblies;AsymmetricKeys;BrokerPriorities;Certificates;ColumnEncryptionKeys;ColumnMasterKeys;Contracts;DatabaseRoles;DatabaseTriggers;Defaults;ExtendedProperties;ExternalDataSources;ExternalFileFormats;ExternalTables;Filegroups;FileTables;FullTextCatalogs;FullTextStoplists;MessageTypes;PartitionFunctions;PartitionSchemes;Permissions;Queues;RemoteServiceBindings;RoleMembership;Rules;ScalarValuedFunctions;SearchPropertyLists;SecurityPolicies;Sequences;Services;Signatures;StoredProcedures;SymmetricKeys;Synonyms;TableValuedFunctions;UserDefinedDataTypes;UserDefinedTableTypes;ClrUserDefinedTypes;Users;Views;XmlSchemaCollections;Audits;Credentials;CryptographicProviders;DatabaseAuditSpecifications;DatabaseScopedCredentials;Endpoints;ErrorMessages;EventNotifications;EventSessions;LinkedServerLogins;LinkedServers;Logins;Routes;ServerAuditSpecifications;ServerRoleMembership;ServerRoles;ServerTriggers'
    #     Invoke-SqlCmd -ServerInstance $server -Database $database -Query 'exec sp_MSforeachtable @command1="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all";'
    # }
    $database = 'CORPORATESMBMODEL'
    $dbtest = 'CORPORATESMBMODELTEST'
    Invoke-SqlCmd -ServerInstance $server -Database $dbtest -Query 'EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all";'
    & "C:/Program Files/Microsoft SQL Server/150/DAC/bin/SqlPackage.exe" /Action:Publish /TargetServerName:$server /TargetDatabaseName:$dbtest /TargetUser:$username /TargetPassword:$password /SourceFile:$dir_path"\DbBackups\$database.dacpac" /p:CreateNewDatabase=false /p:ScriptNewConstraintValidation=true /p:IgnoreWithNocheckOnForeignKeys=true /p:AllowIncompatiblePlatform=true /p:ExcludeObjectTypes='Users;Aggregates;ApplicationRoles;Assemblies;AsymmetricKeys;BrokerPriorities;Certificates;ColumnEncryptionKeys;ColumnMasterKeys;Contracts;DatabaseRoles;DatabaseTriggers;Defaults;ExtendedProperties;ExternalDataSources;ExternalFileFormats;ExternalTables;Filegroups;FileTables;FullTextCatalogs;FullTextStoplists;MessageTypes;PartitionFunctions;PartitionSchemes;Permissions;Queues;RemoteServiceBindings;RoleMembership;Rules;ScalarValuedFunctions;SearchPropertyLists;SecurityPolicies;Sequences;Services;Signatures;StoredProcedures;SymmetricKeys;Synonyms;TableValuedFunctions;UserDefinedDataTypes;UserDefinedTableTypes;ClrUserDefinedTypes;Users;Views;XmlSchemaCollections;Audits;Credentials;CryptographicProviders;DatabaseAuditSpecifications;DatabaseScopedCredentials;Endpoints;ErrorMessages;EventNotifications;EventSessions;LinkedServerLogins;LinkedServers;Logins;Routes;ServerAuditSpecifications;ServerRoleMembership;ServerRoles;ServerTriggers'
    Invoke-SqlCmd -ServerInstance $server -Database $dbtest -Query 'exec sp_MSforeachtable @command1="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all";'

    # # Fix bucket references in Tenant Actual
    # Invoke-SqlCmd -ServerInstance $server -Database 'TENANT_ACTUAL' -Query "UPDATE ThemeRef SET JsCloudLink = REPLACE(JscloudLInk, '"$source_bucket"', '"$target_bucket"');" -Verbose
    Invoke-SqlCmd -ServerInstance $server -Database $dbtest -Query "UPDATE Community SET Description = REPLACE(Description, '"$source_bucket"', '"$target_bucket"');" -Verbose

    # Clean up working directory
    Remove-Item "$dir_path\DbBackups" -Recurse

    # Empty C: recycle bin
    Clear-RecycleBin -DriveLetter C -Force
}
else
{
    Write-Output "Backup file not found in S3, unsafe to proceed with restore"
    throw (New-Object System.IO.FileNotFoundException("File not found: $target_bucket/Rollback/$backup_rollback.zip", $target_bucket, $backup_rollback))
}