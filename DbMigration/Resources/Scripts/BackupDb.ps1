# Set Parameters
param ($current_date_time, $secret_id, $cross_bucket)

# Set region as us-east-1
$default_region = 'Set-DefaultAWSRegion -Region us-east-1'
Invoke-Expression $default_region

# Pull secret with Db information
$secret_json = Get-SECSecretValue -SecretId $secret_id
$secret = $secret_json.SecretString | ConvertFrom-Json

# Set secret values as variables
$username = $secret.username
$password = $secret.password
$database_list = $secret.dblist.Split("-")
$host_name = $secret.host
$server = $secret.server
$dir_path = $secret.dirpath

# Set file names for latest backup and rollback
$backup_latest = $host_name + '_Latest'
$backup_rollback = $host_name + '_' + $current_date_time

# Create working directory
New-Item -Path $dir_path -Name "DbBackups" -ItemType "directory"

# Run sqlpackage extract on each Db
foreach ($database in $database_list)
{
    & "C:/Program Files/Microsoft SQL Server/150/DAC/bin/SqlPackage.exe" /Action:Extract /SourceServerName:$server /SourceDatabaseName:$database /SourceUser:$username /SourcePassword:$password /TargetFile:$dir_path\DbBackups\$database.dacpac /p:ExtractAllTableData=true /p:VerifyExtraction=false /p:CommandTimeout=300 /p:ExtractReferencedServerScopedElements=False /p:IgnoreUserLoginMappings=True
}

# Zip all backups
Compress-Archive -Path "$dir_path\DbBackups\*.*" -DestinationPath "$dir_path\DbBackups\DbBackups.zip"

# Push zipped backups to cross account S3 bucket /Latest & /Rollback
Write-S3Object -BucketName $cross_bucket -File "$dir_path\DbBackups\DbBackups.zip" -Key "/Latest/Backup/$backup_latest.zip"
Write-S3Object -BucketName $cross_bucket -File "$dir_path\DbBackups\DbBackups.zip" -Key "/Rollback/$backup_rollback.zip"

# Clean up working directory
Remove-Item "$dir_path\DbBackups" -Recurse

# Empty C: recycle bin
Clear-RecycleBin -DriveLetter C -Force