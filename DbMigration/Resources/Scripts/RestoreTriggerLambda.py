# CODEBUILD_PROJECT
import boto3
import os

def lambda_handler(event, context):
    codebuild_agent = boto3.client('codebuild')
    codebuild_agent.start_build(
        projectName=os.environ['CODEBUILD_PROJECT'],
    )