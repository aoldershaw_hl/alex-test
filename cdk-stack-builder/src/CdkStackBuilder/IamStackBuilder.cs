using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using JsonTool;

namespace IamStackBuilder
{
    public class IamStack : Stack
    {
        internal IamStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra stack creation logic:
            var statement1 = IamCreatePolicyStatementFromJson("Templates/ec2policy.json");
            var statement2 = IamCreatePolicyStatementFromJson("Templates/ec2policy.json");
            Amazon.CDK.AWS.IAM.PolicyStatement[] statements = { statement1, statement2 };
            var policyDoc = IamCreatePolicyDocument(statements);
            var iamRole = IamCreateRole("testrole", policyDoc);
            var serviceRole = IamCreateServiceLinkedRole("aws:service:name", "test");

            // IAM -
            Amazon.CDK.AWS.IAM.PolicyStatement IamCreatePolicyStatementFromJson(string FILE_PATH) {
                var statementJson = Json.ParseFile(FILE_PATH);
                PolicyStatement statement = PolicyStatement.FromJson(statementJson);  
                return statement;
            }

            Amazon.CDK.AWS.IAM.PolicyDocument IamCreatePolicyDocument(Amazon.CDK.AWS.IAM.PolicyStatement[] POLICY_STATEMENTS) {
                var document = new PolicyDocument();
                document.AddStatements(POLICY_STATEMENTS);
                return document;
            }

            Amazon.CDK.AWS.IAM.CfnRole IamCreateRole(string ROLE_NAME, Amazon.CDK.AWS.IAM.PolicyDocument POLICY_DOCUMENT) {
                var role = new CfnRole(this, ROLE_NAME, new CfnRoleProps{
                    RoleName = ROLE_NAME,
                    AssumeRolePolicyDocument = POLICY_DOCUMENT
                });
                return role;
            }

            Amazon.CDK.AWS.IAM.CfnServiceLinkedRole IamCreateServiceLinkedRole(string AWS_SERVICE_NAME, string ROLE_SUFFIX) {
                var serviceRole = new CfnServiceLinkedRole(this, $"{AWS_SERVICE_NAME}{ROLE_SUFFIX}", new CfnServiceLinkedRoleProps {
                    AwsServiceName = AWS_SERVICE_NAME,
                    CustomSuffix = ROLE_SUFFIX
                });
                return serviceRole;
            } 
        }
    }
}
