using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.KMS;

namespace KmsStackBuilder
{
    public class KmsStack : Stack
    {
        internal KmsStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra stack creation logic:
            var kmsKey = KmsCreateKey("myKey", false);
            
            // KMS - 
            Amazon.CDK.AWS.KMS.IKey KmsCreateKey(string KEY_NAME, bool ENABLE_ROTATION) {
                var key = new Key(this, KEY_NAME, new KeyProps {
                    EnableKeyRotation = ENABLE_ROTATION
                });
                return key;
            }
        }
    }
}
