using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.SSM;
using JsonTool;

namespace SsmStackBuilder
{
    public class SsmStack : Stack
    {
        internal SsmStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra stack creation logic:
            var ssmDoc = DocumentFromJson("myDoc", "Templates/ssmdocument.json");

            // SSM - SsmDocumentFromJson("DOCUMENT_NAME", "FILE_PATH");
            Amazon.CDK.AWS.SSM.CfnDocument DocumentFromJson(string DOCUMENT_NAME, string FILE_PATH) {
                var content = Json.ParseFile(FILE_PATH);
                var document = new CfnDocument(this, "SsmDocument", new CfnDocumentProps
                {
                    Content = content,
                    Name = DOCUMENT_NAME
                });
                return document; 
            }
        }
    }
}
