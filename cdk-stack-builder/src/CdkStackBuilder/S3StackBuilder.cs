using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.KMS;
using Amazon.CDK.AWS.S3;
using Amazon.CDK.AWS.IAM;
using JsonTool;

namespace S3StackBuilder
{
    public class S3Stack : Stack
    {
        internal S3Stack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra stack creation logic:
            var kmsKey = KmsCreateKey("myKey", false);
            var s3Bucket = S3CreateEncryptedBucket("myBucket", true, kmsKey);
            AddBucketPolicy(s3Bucket);

            // S3 - 
            Amazon.CDK.AWS.S3.Bucket S3CreateEncryptedBucket(string BUCKET_NAME, bool VERSIONED, Amazon.CDK.AWS.KMS.IKey KMS_KEY) {
                var s3Bucket = new Bucket(this, BUCKET_NAME, new BucketProps {
                    Versioned = VERSIONED,
                    Encryption = BucketEncryption.KMS,
                    EncryptionKey = KMS_KEY
                });
                return s3Bucket;
            }

            Amazon.CDK.AWS.IAM.IAddToResourcePolicyResult AddBucketPolicy(Amazon.CDK.AWS.S3.Bucket BUCKET) {
                string accountId = "1234567890";
                var policy = s3Bucket.AddToResourcePolicy(new PolicyStatement(new PolicyStatementProps
                {
                    Effect = Effect.ALLOW,
                    Actions = new string[] { "s3:SomeAction" },
                    Resources = new string[] { s3Bucket.BucketArn },
                    Principals = new IPrincipal[] { new AccountPrincipal(accountId) }
                }));
                return policy;
            }
            
            // KMS - 
            Amazon.CDK.AWS.KMS.IKey KmsCreateKey(string KEY_NAME, bool ENABLE_ROTATION) {
                var key = new Key(this, KEY_NAME, new KeyProps {
                    EnableKeyRotation = ENABLE_ROTATION
                });
                return key;
            }

            // // Construct policy inline
            // string accountId = "1234567890";
            // s3Bucket.AddToResourcePolicy(new PolicyStatement(new PolicyStatementProps
            // {
            //     Effect = Effect.ALLOW,
            //     Actions = new string[] { "s3:SomeAction" },
            //     Resources = new string[] { s3Bucket.BucketArn },
            //     Principals = new IPrincipal[] { new AccountPrincipal(accountId) }
            // }));
        }
    }
}
