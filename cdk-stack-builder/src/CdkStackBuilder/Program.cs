﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;
// Import service classes
using SsmStackBuilder;
using IamStackBuilder;
using S3StackBuilder;
using KmsStackBuilder;

namespace Services
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            new SsmStack(app, "SsmStack", new StackProps { Env = makeEnv() });
            new IamStack(app, "IamStack", new StackProps { Env = makeEnv() });
            new KmsStack(app, "KmsStack", new StackProps { Env = makeEnv() });
            new S3Stack(app, "S3Stack", new StackProps { Env = makeEnv() });
            app.Synth();
            
            Amazon.CDK.Environment makeEnv(string account = null, string region = null)
            {
                return new Amazon.CDK.Environment
                {
                    Account = account ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_ACCOUNT"),
                    Region = region ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_REGION")
                };
            }
        }
    }
}
