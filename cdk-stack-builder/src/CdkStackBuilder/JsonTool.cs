using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonTool
{
    public class Json { 
        public static object ParseFile(string filePath) {
            JObject jsonObj = JObject.Parse(File.ReadAllText(filePath));
            return jsonObj;
        }
    }
}
