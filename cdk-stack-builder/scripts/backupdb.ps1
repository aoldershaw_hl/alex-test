$Db_Name = "DB_NAME"
$Bucket = "BUCKET_NAME"
$Now = (Get-Date).ToString('MM-dd-yyyy-hh-mm-ss')
$Backup = "$Db_Name$Now"
$Path = "c:\migration_backups"
New-Item -Path "c:\" -Name "migration_backups" -ItemType "directory"
Backup-SqlDatabase -ServerInstance "." -Database $Db_Name -BackupFile "$Path.bak"
Compress-Archive -LiteralPath "$Path\$Backup.bak" -DestinationPath "$Path\$Backup.zip"
Write-S3Object -BucketName BUCKET_NAME -File "$Path\$Backup.zip" -Key "/$Backup.zip"
Remove-Item "$Path" -Recurse