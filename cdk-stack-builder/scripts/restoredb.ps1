# Restore - need to figure out how to pull the name of the backup from the trigger
$Backup = "$Db_Name$Now"
$Path = "c:\migration_backups"
Read-S3Object -BucketName $Bucket -File "$Path\$Backup.zip" -Key "/$Backup.zip"
Expand-Archive -LiteralPath "$Path\$Backup.zip" -DestinationPath "$Path\$Backup.bak"
Restore-SqlDatabase -ServerInstance "." -Database $Db_Name -BackupFile "$Path\$Backup.bak"