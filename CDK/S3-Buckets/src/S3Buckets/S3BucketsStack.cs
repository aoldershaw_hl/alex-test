using Amazon.CDK;

namespace S3Buckets
{
    public class S3BucketsStack : Stack
    {
        internal S3BucketsStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // The code that defines your stack goes here
        }
    }
}
