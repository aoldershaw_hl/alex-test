﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S3Buckets
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            new S3BucketsStack(app, "S3BucketsStack");
            app.Synth();
        }
    }
}
