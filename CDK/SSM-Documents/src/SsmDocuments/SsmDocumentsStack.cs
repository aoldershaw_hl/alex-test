using Amazon.CDK;
using Amazon.CDK.AWS.SSM;
using System.Collections.Generic;
using System.Linq;
using JsonTool;

namespace SsmDocuments
{
    public class CreateSSMDocumentFromJson : Stack
    {
        internal CreateSSMDocumentFromJson(Construct scope, string id, BuildParams Params, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra creation logic
            var ssmDoc = DocumentFromJson(Params);

            // Methods
            CfnDocument DocumentFromJson(BuildParams Params) {
                var content = Json.ParseFile(Params.FILE_PATH);
                var document = new CfnDocument(this, "SsmDocument", new CfnDocumentProps
                {
                    Content = content,
                    Name = Params.DOCUMENT_NAME
                });
                return document; 
            }
        }
    }
}
