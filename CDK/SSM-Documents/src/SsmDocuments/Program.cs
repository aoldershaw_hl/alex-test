﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SsmDocuments
{
    public class BuildParams
    {
        public string STACK_NAME { get; set; }
        public string DOCUMENT_NAME { get; set; }
        public string FILE_PATH { get; set; }
    }
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();

            // Define parameters
            BuildParams documentParams = new BuildParams{};
            documentParams.STACK_NAME = "teststack";
            documentParams.DOCUMENT_NAME = "newdoc";
            documentParams.FILE_PATH = "templates/ssmdocument.json";

            // Infra creation
            new CreateSSMDocumentFromJson(app, documentParams.STACK_NAME, documentParams, new StackProps { Env = makeEnv() });
            
            app.Synth();

            Amazon.CDK.Environment makeEnv(string account = null, string region = null)
            {
                return new Amazon.CDK.Environment
                {
                    Account = account ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_ACCOUNT"),
                    Region = region ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_REGION")
                };
            }
        }
    }
}
