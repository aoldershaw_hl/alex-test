﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IamRoles
{
    // Set defaults for parameters
    public class BuildParams
    {
        public string STACK_NAME { get; set; }
        public string ROLE_NAME { get; set; }
        public string SERVICE_NAME { get; set; }
        public string ROLE_SUFFIX { get; set; }
        public string FILE_PATH { get; set; }
    }
    public class StatementParams    {
        public string SID { get; set; }
        public string[] ACTIONS { get; set; }
        public string[] RESOURCES { get; set; }
        public string PRINCIPAL { get; set; }
        public string EFFECT { get; set; } = "allow";
    }
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();

            // Create parameters
            BuildParams roleParams = new BuildParams{};
            BuildParams serviceRoleParams = new BuildParams{};
            StatementParams statementParams0 = new StatementParams{};
            StatementParams statementParams1 = new StatementParams{};
            StatementParams statementParams2 = new StatementParams{};

            roleParams.STACK_NAME = "MediaConvertRole";
            roleParams.ROLE_NAME = "MediaConvert_Role";

            serviceRoleParams.STACK_NAME = "MediaConvertServiceRole";
            serviceRoleParams.SERVICE_NAME = "mediaconvert.amazonaws.com";
            serviceRoleParams.ROLE_SUFFIX = "";

            statementParams0.SID = "STSAssumeRole";
            statementParams0.ACTIONS = new string[] { "sts:AssumeRole" };
            statementParams0.PRINCIPAL = "mediaconvert.amazonaws.com";

            statementParams1.SID = "MediaConvertStatement1";
            statementParams1.ACTIONS = new string[] { "s3:Get*", "s3:List*" };
            statementParams1.RESOURCES = new string [] { "arn:aws:s3:::higherlogic-dev-holdingpen/*" };

            statementParams2.SID = "MediaConvertStatement2";
            statementParams2.ACTIONS = new string[] { "s3:Put*" };
            statementParams2.RESOURCES = new string [] { "arn:aws:s3:::higherlogic-dev-stream/*" };

            IamRoles.StatementParams[] SParams = new IamRoles.StatementParams[] { statementParams1, statementParams2 };

            // Create Infra
            new CreateIAMRole(app, roleParams.STACK_NAME, roleParams, SParams, statementParams0, new StackProps { Env = makeEnv() });
            // new CreateIAMServiceRole(app, serviceRoleParams.STACK_NAME, serviceRoleParams, new StackProps { Env = makeEnv() });

            app.Synth();

            Amazon.CDK.Environment makeEnv(string account = null, string region = null)
            {
                return new Amazon.CDK.Environment
                {
                    Account = account ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_ACCOUNT"),
                    Region = region ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_REGION")
                };
            }
        }
    }
}
