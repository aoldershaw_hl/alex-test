using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using JsonTool;

namespace IamRoles
{
    public class CreateIAMRole : Stack
    {
        internal CreateIAMRole(Construct scope, string id, BuildParams Params, StatementParams[] SParams, StatementParams AssumeStatementParams, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra creation logic
            var assumeStatement = CreateAllowStatement(AssumeStatementParams);
            var statements = CreateStatements(SParams);
            var policy = CreateAssumeDocument(assumeStatement);
            var statementDoc = CreatePolicyDocument(statements);
            IDictionary<string, Amazon.CDK.AWS.IAM.PolicyDocument> d = new Dictionary<string, Amazon.CDK.AWS.IAM.PolicyDocument>();
            d.Add($"{Params.ROLE_NAME}_Policy", statementDoc);
            var role = CreateRole(policy, d);

            // Methods
            List<PolicyStatement> CreateStatements(StatementParams[] SParams) {
                List<PolicyStatement> statements = new List<PolicyStatement>();
                foreach (var SParam in SParams)
                {
                    if (SParam.EFFECT.ToLower() == "deny") 
                    {
                        statements.Add(CreateDenyStatement(SParam));
                    }
                    else 
                    {
                        statements.Add(CreateAllowStatement(SParam));
                    } 
                }
                return statements;
            }

            PolicyStatement CreateAllowStatement(StatementParams STATEMENT) {
                var statement = new PolicyStatement(new PolicyStatementProps {
                    Sid = STATEMENT.SID,
                    Effect = Effect.ALLOW,
                });
                statement.AddActions(STATEMENT.ACTIONS);
                if (!String.IsNullOrEmpty(STATEMENT.PRINCIPAL)) {
                    statement.AddServicePrincipal(STATEMENT.PRINCIPAL);
                }
                statement.AddResources(STATEMENT.RESOURCES);
                return statement;
            }

            PolicyStatement CreateDenyStatement(StatementParams STATEMENT) {
                var statement = new PolicyStatement(new PolicyStatementProps {
                    Sid = STATEMENT.SID,
                    Effect = Effect.DENY,
                });
                statement.AddActions(STATEMENT.ACTIONS);
                if (!String.IsNullOrEmpty(STATEMENT.PRINCIPAL)) {
                    statement.AddServicePrincipal(STATEMENT.PRINCIPAL);
                }
                statement.AddResources(STATEMENT.RESOURCES);
                return statement;
            }

            PolicyDocument CreatePolicyDocument(List<PolicyStatement> POLICY_STATEMENTS) {
                PolicyStatement[] POLICY_STATEMENT_ARRAY = POLICY_STATEMENTS.ToArray();
                var document = new PolicyDocument();
                document.AddStatements(POLICY_STATEMENT_ARRAY);
                return document;
            }

            PolicyDocument CreateAssumeDocument(PolicyStatement POLICY_STATEMENT) {
                var document = new PolicyDocument();
                document.AddStatements(POLICY_STATEMENT);
                return document;
            }

            Role CreateRole(PolicyDocument POLICY_DOCUMENT, IDictionary<string, PolicyDocument> POLICY_STATEMENTS) {
                var role = new Role(this, Params.ROLE_NAME, new RoleProps{
                    RoleName = Params.ROLE_NAME,
                    InlinePolicies = POLICY_STATEMENTS,
                    AssumedBy = new ServicePrincipal("mediaconvert.amazonaws.com"),
                });
                return role;
            }

            Role CreateTaskRole() 
            {
                var taskRole = new Role(this, "MediaConvertRole", new RoleProps()
                { 
                    AssumedBy = new ServicePrincipal("mediaconvert.amazonaws.com"),
                    ManagedPolicies = new IManagedPolicy[]
                    {
                        ManagedPolicy.FromAwsManagedPolicyName("AmazonS3FullAccess"),
                    }
                });
                return taskRole;
            }
        }
    }
    public class CreateIAMServiceRole : Stack
    {
        internal CreateIAMServiceRole(Construct scope, string id, BuildParams Params, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra creation logic
            var serviceRole = CreateServiceLinkedRole(Params);
            
            // Methods
            CfnServiceLinkedRole CreateServiceLinkedRole(BuildParams Params) {
                var serviceRole = new CfnServiceLinkedRole(this, $"{Params.SERVICE_NAME}{Params.ROLE_SUFFIX}", new CfnServiceLinkedRoleProps {
                    AwsServiceName = Params.SERVICE_NAME,
                    CustomSuffix = Params.ROLE_SUFFIX
                });
                return serviceRole;
            } 
        }
    }
}
