﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KmsKeys
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            new KmsKeysStack(app, "KmsKeysStack");
            app.Synth();
        }
    }
}
