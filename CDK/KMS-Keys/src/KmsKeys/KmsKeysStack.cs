using Amazon.CDK;

namespace KmsKeys
{
    public class KmsKeysStack : Stack
    {
        internal KmsKeysStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // The code that defines your stack goes here
        }
    }
}
