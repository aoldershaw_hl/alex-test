﻿using Amazon.CDK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sqs
{
    // Set defaults for parameters
    public class BuildParams
    {
        public string TargetVpc { get; set; } = "VPCId";
        public string StackName { get; set; } = "MediaConvertEventQueue";
        public string[] QueuePrefixes { get; set; } = new string[] { "MediaConvertEvent" };
        public string[] QueueSuffixes { get; set; } = new string[] { "" };
    }
    sealed class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            // Create parameters
            BuildParams sqsParams = new BuildParams{};
            Amazon.CDK.Environment makeEnv(string account = null, string region = null)
            {
                return new Amazon.CDK.Environment
                {
                    Account = account ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_ACCOUNT"),
                    Region = region ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_REGION")
                };
            }
            new SqsQueuesStack(app, sqsParams.StackName, sqsParams, new StackProps { Env = makeEnv() });
            app.Synth();
        }
    }
}
