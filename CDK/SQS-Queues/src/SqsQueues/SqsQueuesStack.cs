using Amazon.CDK;
using Amazon.CDK.AWS.SQS;
using Amazon.CDK.AWS.EC2;
using System;
using System.IO;
using System.Collections.Generic;

namespace Sqs
{
    public class SqsQueuesStack : Stack
    {
        internal SqsQueuesStack(Construct scope, string id, BuildParams Params, IStackProps props = null) : base(scope, id, props)
        {
            // Lookup VPC for Queue deployment
            var vpc = Vpc.FromLookup(this, Params.TargetVpc, new VpcLookupOptions
            {
                IsDefault = false,
                VpcId = Params.TargetVpc
            });

            // Loop through Queue prefixes & suffixes to get Queue names: $prefix$suffix & $prefix$suffix_Dead
            foreach (string prefix in Params.QueuePrefixes)
            {
                foreach (string suffix in Params.QueueSuffixes)
                {
                    // Create $prefix$suffix Queue
                    var deadqueue = new Queue(this, prefix + suffix + "_Dead", new QueueProps
                    {
                        QueueName = prefix + suffix + "_Dead",
                        Encryption = QueueEncryption.KMS_MANAGED,
                        RetentionPeriod = Duration.Days(14),

                    });
                    // Create $prefix$suffix_Dead Dead Letter Queue
                    var dlq = new DeadLetterQueue { MaxReceiveCount = 5, Queue = deadqueue };
                    new Queue(this, prefix + suffix, new QueueProps
                    {
                        QueueName = prefix + suffix,
                        Encryption = QueueEncryption.KMS_MANAGED,
                        RetentionPeriod = Duration.Days(14),
                        DeadLetterQueue = dlq,

                    });
                }
            }           
        }
    }
}
