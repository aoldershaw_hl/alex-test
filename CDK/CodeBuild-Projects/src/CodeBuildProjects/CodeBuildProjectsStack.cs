using Amazon.CDK;
using Amazon.CDK.AWS.CodeBuild;
using Amazon.CDK.AWS.IAM;
using System.Collections.Generic;
using System.Linq;
using JsonTool;

namespace CodeBuildProjects
{
    public class CreateProjectFromJson : Stack
    {
        internal CreateProjectFromJson(Construct scope, string id, BuildParams[] ParamsArray, IStackProps props = null) : base(scope, id, props)
        {
            // Main infra creation logic
            foreach (BuildParams Params in ParamsArray)
            {
                var codebuildProject = ProjectFromJson(Params);
            }

            // Methods
            // Creates a Codebuild Project from a given buildspec.json file, env vars, image, and role
            Project ProjectFromJson(BuildParams Params) {
                var content = Json.ParseFile(Params.FILE_PATH);
                var role = Role.FromRoleArn(this,$"{Params.PROJECT_NAME}Role",Params.ROLE_ARN);
                IDictionary<string, object> spec = new Dictionary<string, object>();
                spec.Add($"{Params.PROJECT_NAME}BuildSpec", content);
                var project = new Project(this, Params.PROJECT_NAME, new ProjectProps
                {
                    ProjectName = Params.PROJECT_NAME,
                    BuildSpec = BuildSpec.FromObject(spec),
                    Role = role,
                    Environment = new BuildEnvironment
                    {
                        BuildImage = Params.BUILD_IMAGE,
                        EnvironmentVariables = Params.ENV_VARS
                    }
                });
                return project; 
            }
        }
    }
}
