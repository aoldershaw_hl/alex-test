﻿using Amazon.CDK;
using Amazon.CDK.AWS.CodeBuild;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeBuildProjects
{
    public class BuildParams
    {
        public string STACK_NAME { get; set; }
        public string PROJECT_NAME { get; set; }
        public string FILE_PATH { get; set; }
        public string ROLE_ARN { get; set; }
        public IBuildImage BUILD_IMAGE { get; set; }
        public IDictionary<string, IBuildEnvironmentVariable> ENV_VARS { get; set; }
    }
    sealed class Program
    {
        public static void Main(string[] args)
        {
            // New app
            var app = new App();

            // Define variables
            string env = "prodca";
            string stackName = "ModelDbMigrationStack";
            string serviceRole = "";
            
            // Define build params
            BuildParams backupParams = new BuildParams{};
            backupParams.FILE_PATH = "templates/BackupBuildSpec.json";
            backupParams.STACK_NAME = stackName;
            backupParams.PROJECT_NAME = $"{env}-codebuild-dbmigration-backup";
            backupParams.ROLE_ARN = serviceRole;
            backupParams.BUILD_IMAGE = LinuxBuildImage.STANDARD_4_0;
            backupParams.ENV_VARS = new Dictionary<string, IBuildEnvironmentVariable>
            {
                ["INSTANCE"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["THEMESOURCE"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["MODELSSOURCE"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["SECRETID"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["CROSSBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["REGION"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["DIRPATH"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT }
            };

            BuildParams restoreParams = new BuildParams{};
            restoreParams.FILE_PATH = "templates/RestoreBuildSpec.json";
            restoreParams.STACK_NAME = stackName;
            restoreParams.PROJECT_NAME = $"{env}-codebuild-dbmigration-restore";
            restoreParams.ROLE_ARN = serviceRole;
            restoreParams.BUILD_IMAGE = LinuxBuildImage.STANDARD_4_0;
            restoreParams.ENV_VARS = new Dictionary<string, IBuildEnvironmentVariable>
            {
                ["INSTANCE"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["SOURCEBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["TARGETBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["SECRETID"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["CROSSBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["REGION"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["DIRPATH"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT }
            };

            BuildParams rollbackParams = new BuildParams{};
            rollbackParams.FILE_PATH = "templates/RollbackBuildSpec.json";
            rollbackParams.STACK_NAME = stackName;
            rollbackParams.PROJECT_NAME = $"{env}-codebuild-dbmigration-rollback";
            rollbackParams.ROLE_ARN = serviceRole;
            rollbackParams.BUILD_IMAGE = LinuxBuildImage.STANDARD_4_0;
            rollbackParams.ENV_VARS = new Dictionary<string, IBuildEnvironmentVariable>
            {
                ["CROSSBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["SOURCEBUCKET"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["TIMESTAMP"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT },
                ["SERVER"] = new BuildEnvironmentVariable { Value = " ", Type = BuildEnvironmentVariableType.PLAINTEXT }
            };

            BuildParams[] buildParamArray = { backupParams, restoreParams, rollbackParams };

            // Use infra classes
            new CreateProjectFromJson(app, backupParams.STACK_NAME, buildParamArray, new StackProps { Env = makeEnv() });

            // Infra creation
            app.Synth();

            // Create env
            Amazon.CDK.Environment makeEnv(string account = null, string region = null)
            {
                return new Amazon.CDK.Environment
                {
                    Account = account ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_ACCOUNT"),
                    Region = region ?? System.Environment.GetEnvironmentVariable("CDK_DEFAULT_REGION")
                };
            }
        }
    }
}
