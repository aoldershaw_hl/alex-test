import boto3
import re

def lambda_handler(event, context):
    print(event)
    ec2 = boto3.resource('ec2')
    instanceid = event['detail']['EC2InstanceId']
    print('Instance to be terminated: ' + instanceid) 
    #instanceid = "i-23f97835"
    instance = ec2.Instance(instanceid)
    output = instance.console_output()['Output']
    hostname = re.search('(WIN-[\w\d]{11})',output).group(0)

    ec2details = boto3.client('ec2')
    dcinstance = ec2details.describe_instances(
    Filters=[{
            'Name': 'tag-value',
            'Values': ['DC03',]
        	}])
    if(bool(dcinstance['Reservations'])):
        dcinstanceid = dcinstance["Reservations"][0]["Instances"][0]["InstanceId"]
    else:
        dcinstanceid = False

    if(dcinstanceid == False):
        return 'no domain controller found' 
    else:
        command = 'Remove-ADComputer -Identity "CN=' + hostname + ',OU=FileJob,OU=ServiceASG,OU=Prod,OU=Servers,DC=higherlogic,DC=com" -Server dc03.higherlogic.com -CONFIRM:$false'
        ssm = boto3.client('ssm')
        ssmresponse = ssm.send_command(InstanceIds=[dcinstanceid], DocumentName='AWS-RunPowerShellScript', Parameters= { 'commands': [command] } ) 
        print(ssmresponse)
