import boto3
import re
import json
from datetime import datetime
import sys


def lambda_handler(event, context):
    data = {}
    message = {}
    
    #get datetime stamp
    date = datetime.utcnow()
    
    #see if both values exist
    #if they do
    #if 'MailingID' in event and 'Email' in event:
    if event['MailingID'] != "" and event['Email'] != "":
        #check values
        try:
            mailingid = event['MailingID']
            email = event['Email']
        except:
            print(('Problem fetching parameters: ',sys.exc_info()[0]))
            raise Exception('Error: Problem fetching parameters.')
        #if values pass
        try:
            if(mailingid.isdigit() == False):
                print('Denied: Bad id')
                raise Exception('Denied: bad id: ' + str(id))
            if(isinstance(email, str) == False):
                print(('Denied: Bad email: ' + str(email)))
                raise Exception('Denied: bad email')
            if not re.match("\w[\w\.\_\-'\+]*@[\w\.-]+\.\w+", email):
                print(('Denied: Bad email: ' + str(email)))
                raise Exception('Denied: bad email')
        except:
            print(('Problem validating parameters: ',sys.exc_info()[0]))
            raise Exception('Error: could not validate parameters')
        try:
            message["CreatedOn"] = date.isoformat()
            message["EmailAddress"]= email
            message["MailingID"] = mailingid
            messageResponse = trackEmail(message)
        except:
            print(('Problem generating message : ',sys.exc_info()[0]))
            raise Exception('Error: Problem generating message.')    
        data["location"] = "https://s3.amazonaws.com/invisiblepixel/image.gif"
    #if they dont
    else:
        print('Error: no parameters')
        raise Exception('Error: no paramaters')
    
    return data
    #end of main method


def trackEmail(message):
    #generate client
    try:
        jsonmessage = json.dumps(message)
        client = boto3.client('sqs')
        #send to sqs
        #response = client.send_message(QueueUrl='https://sqs.us-east-1.amazonaws.com/380337340706/BMViewLambdaTest',MessageBody=jsonmessage)
        response = client.send_message(QueueUrl='https://sqs.us-east-1.amazonaws.com/380337340706/BMView',MessageBody=jsonmessage)
    except:
        print(('Problem adding message to queue: ',sys.exc_info()[0]))
        raise Exception('Error: Problem adding message to queue.')
    return response