import boto3 # boto3 is the main library for AWS-Python interaction. Docs are pretty good if this script needs to be changed
import datetime # Python library for handling datetimes
from dateutil import tz

# Main function that AWS Lambda runs

def lambda_handler(event, context):
    #--------Establish connection to SES client--------
    ses_client = boto3.client('ses')
    send_statistics = ses_client.get_send_statistics()
    # Set connection to DynamoDB table for user settings
    db_client = boto3.client('dynamodb')

    #--------USER_SETTINGS - go into the db and pull the settings for each alert--------
    MailSent_15Minute_NoMail = db_client.get_item( TableName = 'SES-Metric-Settings', Key = { 'Settings-Name':{ 'S':'MailSent-15Minute-NoMail'}},AttributesToGet=['15MinutePeriods','Quota'],)
    # Retrieve the user settings from above
    num_of_periods = int(MailSent_15Minute_NoMail['Item']['15MinutePeriods']['N'])
    num_of_emails = int(MailSent_15Minute_NoMail['Item']['Quota']['N'])

    MailOutage_CoolDown = db_client.get_item( TableName = 'SES-ServiceCount', Key = { 'ServiceName': { 'S':'MailOutage' } }, AttributesToGet=[ 'CoolDownPeriods', 'PeriodsLeft' ])
    # Get period and percetnages for upper bound and lower bound
    CoolDownPeriods = float(MailOutage_CoolDown['Item']['CoolDownPeriods']['N'])
    CoolDownPeriodsLeft = float(MailOutage_CoolDown['Item']['PeriodsLeft']['N'])
    
    data = send_statistics['SendDataPoints']
    #sort the data based on the timestamp since AWS returns a non-ordered list
    data = sorted(data,key= lambda point:point['Timestamp']) 

    #--------USEFUL CONSTANTS - Get the last 25 hours as the minimum. Avoid touching the main data since that can consume a lot of resources--------
    today = data[len(data) - 1]['Timestamp']
    last_25_hours = today - datetime.timedelta(hours=25)
    data_last_25_hours = [data_point for data_point in data if data_point['Timestamp'] > last_25_hours]

    #--------Check if there was no mail sent in the past X number of 15 minute periods--------
    # Isolate relevant time period of data
    lower_date_bound = today - datetime.timedelta(minutes = 15 * num_of_periods)
    periods = [int(send_data_point['DeliveryAttempts']) for send_data_point in data_last_25_hours if send_data_point['Timestamp'] > lower_date_bound]

        # Do __ to the dataset to figure out if an alert needs to be sent
    below_threshold = True
    for datapoint in periods:
        if datapoint >= num_of_emails:
            below_threshold = False
            break

    if(CoolDownPeriodsLeft > 0):
        #reduce it by 1
        CoolDownPeriodsLeft -= 1
        #write back to dynamodb
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriodsLeft)
        updateresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailOutage'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriodsLeft)}})
    #else if percentage is below threshold
    elif below_threshold:
        #alert   
        subject = 'SES MAILING ALERT - Possible Mail Outage'
        message = 'The amount of mail sent over each of the the past ' +num_of_periods+ ' periods is below the specified threshold of '+num_of_emails+ ' emails per period.\nThis could indicate an issue with mail services.'     
        #alert('SES-MAIL ALERT --Potential Mail Outage-- ' + str(datetime.datetime.now().strftime("%m-%d %H:%M:%S")), 'Last ' + str(num_of_periods) + ' 15-minute periods, the amount of email sent in each was less than the specified quota of ' + str(num_of_emails) + '. ')
        alert(subject, message)
        #set CD
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriods)
        setresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailOutage'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriods)}})
    
   
    return 'Done.' # Lambda output if everything compiles

# --------------------HELPER FUNCTIONS-----------------------------

#--------Alert function that sends an email based on the alert with two parameters: subject of the email and the actual message

def alert(subj,msg):
    ses_client = boto3.client('ses')
    response = ses_client.send_email(
        Source='donotreply@connectedcommunity.org',
        Destination={
            'ToAddresses': [
                'devops@higherlogic.com'
                #Change for the right ToAddresses: this is for testing purposes
            ]
        },
        Message={
            'Subject': {
                'Data': subj
            },
            'Body': {
                'Text': {
                    'Data': msg
                }
            }
        }
    )

#--------Converts a list of AWS points into a list of tuples with the hashed datetime output as the key and the specific type 
#         of data needed as the value 

def transform(dataset,mail):
    ret_dataset = []
    for datapoint in dataset:
        rounded_dt = lambda dt: datetime.datetime(dt.year, dt.month, dt.day, dt.hour, 15 * (dt.minute // 15))
        dt = datapoint['Timestamp']
        dt = rounded_dt(dt)
        dt = dt.replace(tzinfo=tz.gettz('UTC'))
        dt = dt.astimezone(tz.gettz('America/New_York'))
        if mail is "Bounces":
            tup = (hash(dt),datapoint["Bounces"])
            ret_dataset.append(tup)
        elif mail is "Complaints":
            tup = (hash(dt),datapoint["Complaints"])
            ret_dataset.append(tup)
        elif mail is "Rejects":
            tup = (hash(dt),datapoint["Rejects"])
            ret_dataset.append(tup)
        else:
            tup = (hash(dt),datapoint["DeliveryAttempts"])
            ret_dataset.append(tup)
    return ret_dataset

# Simple hash function for converting datetimes to a 3 digit number that matches the database

def hash(timestamp):
    if(timestamp.weekday() > 5):
        hashed = int(100 + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    else:
        hashed = int(((timestamp.weekday() + 2) * 100) + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    return hashed