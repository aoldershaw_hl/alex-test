import boto3 # boto3 is the main library for AWS-Python interaction. Docs are pretty good if this script needs to be changed
import datetime # Python library for handling datetimes
from dateutil import tz
# Main function that AWS Lambda runs

def lambda_handler(event, context):
   

    # Set connection to DynamoDB table for user settings
    db_client = boto3.client('dynamodb')
    #--------USER_SETTINGS - go into the db and pull the settings for each alert--------
    MailSent_Hour_UpperBound = db_client.get_item( TableName = 'SES-Metric-Settings', Key = { 'Settings-Name': { 'S':'MailSent-Hour-UpperBound' } }, AttributesToGet=[ 'HourPeriod', 'Percentage' ])
    # Get period and percetnages for upper bound and lower bound
    upperbound_hour_period = float(MailSent_Hour_UpperBound['Item']['HourPeriod']['N'])
    upperbound_percentage = float(MailSent_Hour_UpperBound['Item']['Percentage']['N'])
    
    UpperBound_CoolDown = db_client.get_item( TableName = 'SES-ServiceCount', Key = { 'ServiceName': { 'S':'MailSentUpperBound' } }, AttributesToGet=[ 'CoolDownPeriods', 'PeriodsLeft' ])
    # Get period and percetnages for upper bound and lower bound
    CoolDownPeriods = float(UpperBound_CoolDown['Item']['CoolDownPeriods']['N'])
    CoolDownPeriodsLeft = float(UpperBound_CoolDown['Item']['PeriodsLeft']['N'])

    #--------Establish connection to SES client--------
    ses_client = boto3.client('ses')
    send_statistics = ses_client.get_send_statistics()
    data = send_statistics['SendDataPoints']
    #sort the data based on the timestamp since AWS returns a non-ordered list
    data = sorted(data,key= lambda point:point['Timestamp']) 

    #--------USEFUL CONSTANTS - Get the last 25 hours as the minimum. Avoid touching the main data since that can consume a lot of resources--------
    nowtime = data[len(data) - 1]['Timestamp']
    #rounded_dt = lambda dt: datetime.datetime(dt.year, dt.month, dt.day, dt.hour, 15 * (dt.minute // 15))
    #dt = rounded_dt(nowtime)
    #dt = dt.replace(tzinfo=tz.gettz('UTC'))
    #dt = dt.astimezone(tz.gettz('America/New_York'))
    #nowhash = hash(dt)
    last_25_hours = nowtime- datetime.timedelta(hours=25)
    data_last_25_hours = [data_point for data_point in data if data_point['Timestamp'] > last_25_hours]
    lower_date = nowtime - datetime.timedelta(hours = upperbound_hour_period)
    periods = transform([send_data_point for send_data_point in data_last_25_hours if send_data_point['Timestamp'] > lower_date],"Requests")

    # Look into database to see what is the normal sum for the period
    db_sum = 0.000
    now_sum = 0.000
    runs = 0
    displays = ""
    for (date_hash,value) in periods:
        runs += 1 
        now_sum += value
        db_request = db_client.get_item(TableName = 'SES-Averages',Key = {'DateTimeIntervalHash': {'S': str(date_hash)}},AttributesToGet=['Requests'],)
        db_sum += float(db_request['Item']['Requests']['S'])
        displays += 'Run ' + str(runs) + '('+str(date_hash)+'): Now: ' + str(value) + ' DB: ' + str(float(db_request['Item']['Requests']['S'])) + '\n'


    if(CoolDownPeriodsLeft > 0):
        #reduce it by 1
        CoolDownPeriodsLeft -= 1
        #write back to dynamodb
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriodsLeft)
        updateresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailSentUpperBound'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriodsLeft)}})
    else:
        now_percent = float(now_sum/db_sum)*100
        if now_percent > 100:
            now_percent -= 100
            if now_percent >= (upperbound_percentage):
                #alert 
                subject = 'SES MAILING ALERT - Mail Sent Above Threshold'
                message = 'The amount of email sent over the last '+str(upperbound_hour_period)+' hours was above the specified threshold of ' + str(upperbound_percentage) + '%.\n' +str(now_sum)+ ' emails were sent over the designated period which is '+str(round(now_percent,2))+'% above the normal amount: ' + str(db_sum)
                # message += '\n' + displays + '\n' + str(nowhash) + str(dt)
                #alert('SES-MAIL ALERT ' + str(datetime.datetime.now().strftime("%m-%d %H:%M:%S")), 'Last ' + str(upperbound_hour_period) + ' hours of requests are above the normal ' + str(upperbound_hour_period) + ' hour period by ' + str(round(now_percent,2)) + '% ('+ str(now_sum) +'). Acceptable threshold is ' + str(upperbound_percentage) + '% greater than normal ('+ str(db_sum) +').')
                alert(subject, message)
                #set CD
                updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriods)
                setresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailSentUpperBound'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriods)}})
        
    
    return 'Done.' # Lambda output if everything compiles

# --------------------HELPER FUNCTIONS-----------------------------

#--------Alert function that sends an email based on the alert with two parameters: subject of the email and the actual message

def alert(subj,msg):
    ses_client = boto3.client('ses')
    response = ses_client.send_email(
        Source='donotreply@connectedcommunity.org',
        Destination={
            'ToAddresses': [
                'devops@higherlogic.com'
                #Change for the right ToAddresses: this is for testing purposes
            ]
        },
        Message={
            'Subject': {
                'Data': subj
            },
            'Body': {
                'Text': {
                    'Data': msg
                }
            }
        }
    )

#--------
#        

def checklastalert(alerttype, now):

    db = boto3.client('dynamodb')
    lastalert = db.get_item(
        TableName = 'SES-ServiceCount',
        Key = {
            'ServiceName': {
                'S':alerttype
            }
        },
        AttributesToGet=[
            'LastRun',
            'HourPeriodCD',
            '15MinPeriodCD'
        ],
    )

    lastalerttime = datetime.datetime(lastalert['Item']['LastRun']['S'])
    cdperiods = (int(lastalert['Item']['HourPeriodCD']['S'])*4) + int(lastalert['Item']['15MinPeriodCD']['S'])

    timediff = now-lastalerttime
    periods = timediff.total_minutes()/15
    if periods > cdperiods:
        db.update_item(
            TableName = 'SES-ServiceCount',
            Key = {
                'ServiceName': {
                    'S':alerttype
                }
            },
            UpdateExpression = 'SET LastRun = :val1',
            ExpressionAttributeValues = {
                ':val1': str(now)
            }
        )
        return True
    else:
        return False;
        
    

#--------Converts a list of AWS points into a list of tuples with the hashed datetime output as the key and the specific type 
#         of data needed as the value 

def transform(dataset,mail):
    ret_dataset = []
    for datapoint in dataset:
        rounded_dt = lambda dt: datetime.datetime(dt.year, dt.month, dt.day, dt.hour, 15 * (dt.minute // 15))
        dt = datapoint['Timestamp']
        dt = rounded_dt(dt)
        dt = dt.replace(tzinfo=tz.gettz('UTC'))
        dt = dt.astimezone(tz.gettz('America/New_York'))
        if mail is "Bounces":
            tup = (hash(dt),datapoint["Bounces"])
            ret_dataset.append(tup)
        elif mail is "Complaints":
            tup = (hash(dt),datapoint["Complaints"])
            ret_dataset.append(tup)
        elif mail is "Rejects":
            tup = (hash(dt),datapoint["Rejects"])
            ret_dataset.append(tup)
        else:
            tup = (hash(dt),datapoint["DeliveryAttempts"])
            ret_dataset.append(tup)
    return ret_dataset

# Simple hash function for converting datetimes to a 3 digit number that matches the database

def hash(timestamp):
    if(timestamp.weekday() > 5):
        hashed = int(100 + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    else:
        hashed = int(((timestamp.weekday() + 2) * 100) + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    return hashed