import boto3 # boto3 is the main library for AWS-Python interaction. Docs are pretty good if this script needs to be changed
import datetime # Python library for handling datetimes
from dateutil import tz

# Main function that AWS Lambda runs

def lambda_handler(event, context):
    #--------Establish connection to SES client--------
    ses_client = boto3.client('ses')
    #--------Get Data----------------------------------
    send_quota_metrics = ses_client.get_send_quota()
    max_quota = send_quota_metrics['Max24HourSend']
    current_send_count = send_quota_metrics['SentLast24Hours']
    #-Set connection to DynamoDB table for user settings
    db_client = boto3.client('dynamodb')
    #--------Get Settings------------------------------
    MailSent_24Hour_QuotaBound = db_client.get_item(TableName = 'SES-Metric-Settings', Key = { 'Settings-Name': { 'S':'MailSent-24Hour-QuotaBound' }}, AttributesToGet=[ 'Percentage', 'Quota' ])
    quota_percentage = float(MailSent_24Hour_QuotaBound['Item']['Percentage']['N'])
    #--------Get Current Quota Percentage--------------
    requests_percent = float( current_send_count/max_quota )

    MailOutage_CoolDown = db_client.get_item( TableName = 'SES-ServiceCount', Key = { 'ServiceName': { 'S':'24HourQuota' } }, AttributesToGet=[ 'CoolDownPeriods', 'PeriodsLeft' ])
    # Get period and percetnages for upper bound and lower bound
    CoolDownPeriods = float(MailOutage_CoolDown['Item']['CoolDownPeriods']['N'])
    CoolDownPeriodsLeft = float(MailOutage_CoolDown['Item']['PeriodsLeft']['N'])

    #--------See if percentage has exceed threshold----
    if(CoolDownPeriodsLeft > 0):
        #reduce it by 1
        CoolDownPeriodsLeft -= 1
        #write back to dynamodb
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriodsLeft)
        updateresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'24HourQuota'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriodsLeft)}})
    #else if percentage is below threshold
    elif requests_percent >= (quota_percentage/100):
        #alert
        subject = 'SES MAILING ALERT - Mail Send Rate Approaching Provisioned Quota'
        message = 'The amount of email sent over the last 24 hours is ' +requests_percent+ '% of the alloted quota of ' +max_quota+ ' emails.' 
        #alert('SES-MAIL ALERT ' + str(datetime.datetime.now().strftime("%m-%d %H:%M:%S")), 'The number of mail requests in the past 24 hours is ' + (str(requests_percent-quota_percentage)) + '% higher than the allowable threshold of ' + str(quota_percentage) + '% of the AWS quota: ' + str(max_quota) + '.')
        alert(subject, message)
        #set CD
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriods)
        setresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'24HourQuota'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriods)}})
    

    return 'Done.'

def alert(subj,msg):
    ses_client = boto3.client('ses')
    response = ses_client.send_email(
        Source='donotreply@connectedcommunity.org',
        Destination={
            'ToAddresses': [
                'devops@higherlogic.com'
                #Change for the right ToAddresses: this is for testing purposes
            ]
        },
        Message={
            'Subject': {
                'Data': subj
            },
            'Body': {
                'Text': {
                    'Data': msg
                }
            }
        }
    )