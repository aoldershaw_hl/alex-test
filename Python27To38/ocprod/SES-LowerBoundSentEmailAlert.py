import boto3 # boto3 is the main library for AWS-Python interaction. Docs are pretty good if this script needs to be changed
import datetime # Python library for handling datetimes
from dateutil import tz

# Main function that AWS Lambda runs

def lambda_handler(event, context):
    # Set connection to DynamoDB table for user settings
    db_client = boto3.client('dynamodb')
    #--------USER_SETTINGS - go into the db and pull the settings for each alert--------
    MailSent_Hour_LowerBound = db_client.get_item( TableName = 'SES-Metric-Settings', Key = { 'Settings-Name': { 'S':'MailSent-Hour-LowerBound' } }, AttributesToGet=[ 'HourPeriod', 'Percentage' ])
    # Get period and percetnages for upper bound and lower bound
    lowerbound_hour_period = float(MailSent_Hour_LowerBound['Item']['HourPeriod']['N'])
    lowerbound_percentage = float(MailSent_Hour_LowerBound['Item']['Percentage']['N'])

    LowerBound_CoolDown = db_client.get_item( TableName = 'SES-ServiceCount', Key = { 'ServiceName': { 'S':'MailSentLowerBound' } }, AttributesToGet=[ 'CoolDownPeriods', 'PeriodsLeft' ])
    # Get period and percetnages for upper bound and lower bound
    CoolDownPeriods = float(LowerBound_CoolDown['Item']['CoolDownPeriods']['N'])
    CoolDownPeriodsLeft = float(LowerBound_CoolDown['Item']['PeriodsLeft']['N'])

    #--------Establish connection to SES client--------
    ses_client = boto3.client('ses')
    send_statistics = ses_client.get_send_statistics()
    data = send_statistics['SendDataPoints']
    #sort the data based on the timestamp since AWS returns a non-ordered list
    data = sorted(data, key = lambda point:point['Timestamp']) 

    #--------USEFUL CONSTANTS - Get the last 25 hours as the minimum. Avoid touching the main data since that can consume a lot of resources--------
    today = data[len(data) - 1]['Timestamp']
    last_25_hours = today - datetime.timedelta(hours=25)
    data_last_25_hours = [data_point for data_point in data if data_point['Timestamp'] > last_25_hours]

    lower_date = today - datetime.timedelta(hours = lowerbound_hour_period)
    periods = transform([send_data_point for send_data_point in data_last_25_hours if send_data_point['Timestamp'] > lower_date],"Requests")

    # Look into database to see what is the normal sum for the period
    db_sum = 0.000
    now_sum = 0.000
    runsss = 0
    displays = ""
    for (date_hash,value) in periods:
        runsss += 1 
        now_sum += value
        db_request = db_client.get_item(
            TableName = 'SES-Averages',
            Key = {
                'DateTimeIntervalHash': {
                    'S': str(date_hash)
                }
            },
            AttributesToGet=[
                'Requests'
            ],
        )
        db_sum += float(db_request['Item']['Requests']['S'])
        displays += 'Run ' + str(runsss) + '('+str(date_hash)+'): Now: ' + str(value) + ' DB: ' + str(float(db_request['Item']['Requests']['S'])) + '\n'

    # Get current percentage
    now_percent = float(now_sum/db_sum)*100

    #check cooldown
    #if cooldown > 0
    if(CoolDownPeriodsLeft > 0):
        #reduce it by 1
        CoolDownPeriodsLeft -= 1
        #write back to dynamodb
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriodsLeft)
        updateresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailSentLowerBound'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriodsLeft)}})
    #else if percentage is below threshold
    elif now_percent <= (100-lowerbound_percentage):
        #alert 
        #percent_diff = lowerbound_percentage-now_percent
        subject = 'SES MAILING ALERT - Mail Sent Below Threshold'
        message = 'The amount of email sent over the last '+str(lowerbound_hour_period)+' hours was below the specified threshold of ' + str(lowerbound_percentage) + '% less than average.\n' +str(now_sum)+ ' emails were sent over the designated period which is '+str(round(now_percent,2))+'% of the normal amount: ' + str(db_sum)
        #message += '\n' + displays
        #alert('SES-MAIL ALERT ' + str(datetime.datetime.now().strftime("%m-%d %H:%M:%S")), 'Last ' + str(lowerbound_hour_period) + ' hours of requests are below the normal ' + str(lowerbound_hour_period) + ' hour period by ' + str(round(now_percent,2)) + '% ('+ str(now_sum) +'). Acceptable threshold is ' + str(lowerbound_percentage) + '% less than normal ('+ str(db_sum) +').')
        alert(subject, message)
        #set CD
        updatesyntax = 'SET PeriodsLeft = :num'#+ str(CoolDownPeriods)
        setresponse = db_client.update_item(TableName = 'SES-ServiceCount',Key = {'ServiceName':{'S':'MailSentLowerBound'}},UpdateExpression=updatesyntax, ExpressionAttributeValues={':num':{'N': str(CoolDownPeriods)}})
    
    return 'Done.' # Lambda output if everything compiles

# --------------------HELPER FUNCTIONS-----------------------------

#--------Alert function that sends an email based on the alert with two parameters: subject of the email and the actual message

def alert(subj,msg):
    ses_client = boto3.client('ses')
    response = ses_client.send_email(
        Source='donotreply@connectedcommunity.org',
        Destination={
            'ToAddresses': [
                #Change for the right ToAddresses: this is for testing purposes
                'devops@higherlogic.com'
            ]
        },
        Message={
            'Subject': {
                'Data': subj
            },
            'Body': {
                'Text': {
                    'Data': msg
                }
            }
        }
    )

#--------Converts a list of AWS points into a list of tuples with the hashed datetime output as the key and the specific type 
#         of data needed as the value 

def transform(dataset,mail):
    ret_dataset = []
    for datapoint in dataset:
        rounded_dt = lambda dt: datetime.datetime(dt.year, dt.month, dt.day, dt.hour, 15 * (dt.minute // 15))
        dt = datapoint['Timestamp']
        dt = rounded_dt(dt)
        dt = dt.replace(tzinfo=tz.gettz('UTC'))
        dt = dt.astimezone(tz.gettz('America/New_York'))
        if mail is "Bounces":
            tup = (hash(dt),datapoint["Bounces"])
            ret_dataset.append(tup)
        elif mail is "Complaints":
            tup = (hash(dt),datapoint["Complaints"])
            ret_dataset.append(tup)
        elif mail is "Rejects":
            tup = (hash(dt),datapoint["Rejects"])
            ret_dataset.append(tup)
        else:
            tup = (hash(dt),datapoint["DeliveryAttempts"])
            ret_dataset.append(tup)
    return ret_dataset

# Simple hash function for converting datetimes to a 3 digit number that matches the database

def hash(timestamp):
    if(timestamp.weekday() > 5):
        hashed = int(100 + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    else:
        hashed = int(((timestamp.weekday() + 2) * 100) + ((timestamp.hour * 4) + (timestamp.minute / 15)))
    return hashed