
import logging
import boto3

LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)

def lambda_handler(event, context):
    opsworks_client = boto3.client('opsworkscm', region_name=event['region'])
    resp = opsworks_client.disassociate_node(ServerName='Chef-Automate-02-Prod', NodeName=event['detail']['EC2InstanceId'],
    EngineAttributes=[dict(Name="CHEF_ORGANIZATION", Value="default")])
    if resp['ResponseMetadata']['HTTPStatusCode'] == 200:
        LOG.debug("Disassociated node {}".format(event['detail']['EC2InstanceId']))
    ssm = boto3.client('ssm' )
    instanceid = event['detail']['EC2InstanceId']
    command = 'sudo automate-ctl delete-node -n ' + instanceid + ' --force'
    visibility = ssm.send_command(Targets=[ { "Key":"tag:opsworks-cm:server-name","Values":["Chef-Automate-02-Prod"] } ], DocumentName='AWS-RunShellScript', Parameters= { 'commands': [command] } )