import boto3

def lambda_handler(instance, filter):
    custom_filter = [{
        'Name':'tag:Name',
        'Values': ['WEB'+'?'+'0'+'?']}]

    client = boto3.client('ec2')
    ec2 = boto3.resource('ec2')
    instances = ec2.instances.filter(Filters=custom_filter)


    instancelist = []
    for instance in instances:
        for tag in instance.tags:
                if 'Name' == tag['Key']:
                    name = tag['Value']
        instancelist.append(tuple([name, instance.id])) 


    cloudwatch = boto3.client('cloudwatch')
    for x in instancelist:
        Name, Id = tuple(x)
        cloudwatch.put_metric_alarm(
        AlarmName='Web_Server_CPU_Utilization_%s' % Name,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=10,
        DatapointsToAlarm=7,
        MetricName='CPUUtilization',
        Namespace='AWS/EC2',
        Period=60,
        Statistic='Average',
        Threshold=92.0,
        ActionsEnabled=True,
        AlarmActions=['arn:aws:sns:us-east-1:380337340706:DevOps'],
        AlarmDescription='Alarm when server CPU is too high, too often, for too long',
        TreatMissingData='missing',
        Dimensions=[
            {
              'Name': 'InstanceId',
              'Value': Id
            },
        ],
        )